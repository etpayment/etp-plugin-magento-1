<?php

class ETPay_ETPay_Model_Method extends Mage_Payment_Model_Method_Abstract
{
    protected $_code = 'etpay';

    protected $_formBlockType = 'etpay/form';
    protected $_infoBlockType = 'etpay/info';

    public function getOrderPlaceRedirectUrl() {
	    	$quoteId = Mage::getSingleton('checkout/session')->getQuoteId();
		return Mage::getUrl('etpay/redirect/index', array('_secure' => true, '_query' => 'quoteId='.$quoteId));
	}
}
