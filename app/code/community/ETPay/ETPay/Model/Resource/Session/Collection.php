<?php

class ETPay_ETPay_Model_Resource_Session_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
 {
     public function _construct()
     {
         parent::_construct();
         $this->_init('etpay/session');
     }
}
