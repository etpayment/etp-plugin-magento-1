<?php

class ETPay_ETPay_Model_Resource_Session extends Mage_Core_Model_Resource_Db_Abstract
{
     public function _construct()
     {
         $this->_init('etpay/session', 'id');
     }
}
