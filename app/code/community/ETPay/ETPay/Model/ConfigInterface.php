<?php

/**
 * Interface ConfigInterface
 */
class ETPay_ETPay_Model_ConfigInterface 
{
    const ETPAY_PRODUCTION_ENVIRONMENT = 1;
    const ETPAY_DEVELOPMENT_ENVIRONMENT = 2;
    const ETPAY_REDIRECT_URI = 'etpay/redirect/';
    const ETPAY_IFRAME_DEPLOYMENT = 'iframe';
    const ETPAY_REDIRECT_DEPLOYMENT = 'redirect';

    const XML_PATH_ACTIVE = 'payment/etpay/active';
    const XML_PATH_TITLE = 'payment/etpay/title';
    const XML_PATH_DEPLOYMENT = 'payment/etpay/deployment';
    const XML_PATH_ENVIRONMENT = 'payment/etpay/environment';
    const XML_PATH_URL_API_PRODUCTION = 'payment/etpay/url_api_production';
    const XML_PATH_API_KEY_PRODUCTION = 'payment/etpay/api_key_production';
    const XML_PATH_MERCHANT_CODE_PRODUCTION = 'payment/etpay/merchant_code_production';
    const XML_PATH_URL_PAYMENT_PRODUCTION = 'payment/etpay/url_payment_production';
    const XML_PATH_URL_API_SANDBOX = 'payment/etpay/url_api_sandbox';
    const XML_PATH_API_KEY_SANDBOX = 'payment/etpay/api_key_sandbox';
    const XML_PATH_MERCHANT_CODE_SANDBOX = 'payment/etpay/merchant_code_sandbox';
    const XML_PATH_URL_PAYMENT_SANDBOX = 'payment/etpay/url_payment_sandbox';
    const XML_PATH_DEBUG = 'payment/etpay/debug';
    const XML_PATH_ALLOWSPECIFIC = 'payment/etpay/allowspecific';
    const XML_PATH_SPECIFICCOUNTRY = 'payment/etpay/specificcountry';
    const XML_PATH_SORT_ORDER = 'payment/etpay/sort_order';
}