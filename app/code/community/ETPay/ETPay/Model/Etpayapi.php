<?php

class ETPay_ETPay_Model_Etpayapi extends Mage_Core_Model_Abstract
{
	public function __construct()
	{
        	$this->_init('etpay/etpayapi');
	}

    public function createETPayObject($orderId, $token = NULL)
    {
		$order = Mage::getModel('sales/order')->loadByIncrementId($orderId);

		// Redirect Result URL
		$commerce_url = Mage::getUrl('etpay/result', ['order_id' => $orderId]);
		$KOcommerce_url = Mage::getUrl('etpay/koresult', ['order_id' => $orderId]);
		$OKcommerce_url = Mage::getUrl('etpay/okresult', ['order_id' => $orderId]);
		$deployment = $this->getDeployment();

		// Initialize Session
		$etpaySession = Mage::getModel('etpay/session');

		if ($token) {
			$etpaySession->load($token, 'token');
            if ($etpaySession->getId()) {
                $token = $etpaySession->getToken();
                $signatureToken = $etpaySession->getSignatureToken();
            }
		} 

		if (!$token) {
            $session = $this->initializeSession($order);

			$token = $session['token'];
			$signatureToken = $session['signatureToken'];

			$data = array(
				'token' => $token,
				'signature_token' => $signatureToken
			);
			$etpaySession->setData($data); 

			$etpaySession->save();
		}

		return array(
			"ETPAY_MERCHANT_URL" => $commerce_url,
			"ETPAY_MERCHANT_URL_OK" => $OKcommerce_url,
			"ETPAY_MERCHANT_URL_KO" => $KOcommerce_url,
			"ETPAY_TOKEN" => $token,
			"ETPAY_SIGNATURE_TOKEN" => $signatureToken,
			"ETPAY_DEPLOYMENT" => $deployment
		);
	}

	private function initializeSession($order)
	{
		$url = $this->getAPIUrl();

		$client = new Varien_Http_Client();
		$client
			->setUri($url)
			->setMethod(Zend_Http_Client::POST)
			->setConfig(
				array(
					'maxredirects' => 0,
					'timeout' => 30,
				)
			);

		$headers = array(
			\Zend_Http_Client::CONTENT_TYPE => 'application/json',
			'Accept' =>'application/json'
		);
		$client->setHeaders($headers);

		$params = array(
			'merchant_code' => $this->getMerchantCode(),
			'merchant_api_token' => $this->getAPIKey(),
			'merchant_order_id' => $order->getIncrementId(),
			'order_amount' => $order->getGrandTotal(),
			'customer_email' => $order->getCustomerEmail(),
			'metadata' => array(
				array(
					"name" => 'Cuenta',
					"value" => 1111,
					"show" => true
				)
			)
		);

		$client->setRawData(json_encode($params), "application/json;charset=UTF-8");

		try {
			$response = json_decode($client->request()->getBody());
		}
		catch (Exception $e) {
		   echo $e->getMessage();
		}

		return array(
			'token' => $response->token,
			'signatureToken' => $response->signature_token,
			'termsUrl' => $response->terms_url
		);
	}

	/**
     * @return string
     */
    private function getAPIUrl()
    {
        $route = 'session/initialize';

        $environment = Mage::getStoreConfig(ETPay_ETPay_Model_ConfigInterface::XML_PATH_ENVIRONMENT, Mage::app()->getStore());
        return (ETPay_ETPay_Model_ConfigInterface::ETPAY_PRODUCTION_ENVIRONMENT == $environment) 
            ? Mage::getStoreConfig(ETPay_ETPay_Model_ConfigInterface::XML_PATH_URL_API_PRODUCTION, Mage::app()->getStore()) . $route
            : Mage::getStoreConfig(ETPay_ETPay_Model_ConfigInterface::XML_PATH_URL_API_SANDBOX, Mage::app()->getStore()) . $route;
    }

	/**
     * @return string
     */
    private function getMerchantCode()
    {
        $environment = Mage::getStoreConfig(ETPay_ETPay_Model_ConfigInterface::XML_PATH_ENVIRONMENT, Mage::app()->getStore());
        return (ETPay_ETPay_Model_ConfigInterface::ETPAY_PRODUCTION_ENVIRONMENT == $environment) 
            ? Mage::getStoreConfig(ETPay_ETPay_Model_ConfigInterface::XML_PATH_MERCHANT_CODE_PRODUCTION, Mage::app()->getStore()) 
            : Mage::getStoreConfig(ETPay_ETPay_Model_ConfigInterface::XML_PATH_MERCHANT_CODE_SANDBOX, Mage::app()->getStore());
    }

    /**
     * @return string
     */
    private function getAPIKey()
    {
        $environment = Mage::getStoreConfig(ETPay_ETPay_Model_ConfigInterface::XML_PATH_ENVIRONMENT, Mage::app()->getStore());
        return (ETPay_ETPay_Model_ConfigInterface::ETPAY_PRODUCTION_ENVIRONMENT == $environment) 
            ? Mage::getStoreConfig(ETPay_ETPay_Model_ConfigInterface::XML_PATH_API_KEY_PRODUCTION, Mage::app()->getStore()) 
            : Mage::getStoreConfig(ETPay_ETPay_Model_ConfigInterface::XML_PATH_API_KEY_SANDBOX, Mage::app()->getStore());
    }

	private function getDeployment()
    {
        return Mage::getStoreConfig(ETPay_ETPay_Model_ConfigInterface::XML_PATH_DEPLOYMENT, Mage::app()->getStore());
    }
}
