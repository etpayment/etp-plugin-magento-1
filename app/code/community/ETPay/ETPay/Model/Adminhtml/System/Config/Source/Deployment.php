<?php

class ETPay_ETPay_Model_Adminhtml_System_Config_Source_Deployment
{
   public function toOptionArray()
   {
       $themes = array(
           array('value' => ETPay_ETPay_Model_ConfigInterface::ETPAY_REDIRECT_DEPLOYMENT, 'label' => 'Redirect'),
           array('value' => ETPay_ETPay_Model_ConfigInterface::ETPAY_IFRAME_DEPLOYMENT, 'label' => 'Iframe'),
       );
       return $themes;
   }
}