<?php

class ETPay_ETPay_Model_Adminhtml_System_Config_Source_Environment
{
   public function toOptionArray()
   {
       $themes = array(
           array('value' => ETPay_ETPay_Model_ConfigInterface::ETPAY_DEVELOPMENT_ENVIRONMENT, 'label' => 'Sandbox'),
           array('value' => ETPay_ETPay_Model_ConfigInterface::ETPAY_PRODUCTION_ENVIRONMENT, 'label' => 'Production'),
       );
       return $themes;
   }
}