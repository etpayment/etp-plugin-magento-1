<?php

class ETPay_ETPay_Block_Form extends Mage_Payment_Block_Form
{
    public function __construct()
    {
        parent::_construct();
        $this->setTemplate('etpay/form.phtml');
    }
}
