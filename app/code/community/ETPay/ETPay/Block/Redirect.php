<?php

class ETPay_ETPay_Block_Redirect extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::_construct();
        $this->setTemplate('etpay/redirect.phtml');
    }

    /**
     * @return string
     */
    public function getPaymentUrl($token)
    {
        $route = 'session/'.$token;

        $environment = Mage::getStoreConfig(ETPay_ETPay_Model_ConfigInterface::XML_PATH_ENVIRONMENT, Mage::app()->getStore());
        return (ETPay_ETPay_Model_ConfigInterface::ETPAY_PRODUCTION_ENVIRONMENT == $environment) 
            ? Mage::getStoreConfig(ETPay_ETPay_Model_ConfigInterface::XML_PATH_URL_PAYMENT_PRODUCTION, Mage::app()->getStore()) . $route
            : Mage::getStoreConfig(ETPay_ETPay_Model_ConfigInterface::XML_PATH_URL_PAYMENT_SANDBOX, Mage::app()->getStore()) . $route;
    }

    /**
     * @return string
     */
    public function getDeployment()
    {
        return Mage::getStoreConfig(ETPay_ETPay_Model_ConfigInterface::XML_PATH_DEPLOYMENT, Mage::app()->getStore());
    }
}
