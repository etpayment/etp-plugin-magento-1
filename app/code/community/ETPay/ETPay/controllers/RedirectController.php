<?php

class ETPay_ETPay_RedirectController extends Mage_Core_Controller_Front_Action
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var ETPay_ETPayAPI
     */
    private $api = null;

    /**
     * @var Order
     */
    private $order = null;

    public function preDispatch()
    {
        $this->api = $this->getAPI();

        parent::preDispatch();
    }

    public function indexAction() {
	echo Mage::app()->getLayout()
		        ->createBlock('etpay/redirect')
			->setToken($this->api['ETPAY_TOKEN'])
			->setTemplate('etpay/redirect.phtml')
			->toHtml();
    }

    private function getAPI()
    {
        if (is_null($this->api)) {	
	    $api = Mage::getModel('etpay/etpayapi');
	    $quoteId = $this->getRequest()->getParam('quoteId');
	    $quote = Mage::getModel('sales/quote')->load($quoteId);
	    $orderId = $quote->getReservedOrderId();
	    $this->api = $api->createETPayObject($orderId);
        }
        return $this->api;
    }

    /**
     * @return string
     */
    protected function getPaymentUrl($token)
    {
        $route = 'session/'.$token;

        $environment = Mage::getStoreConfig(ETPay_ETPay_Model_ConfigInterface::XML_PATH_ENVIRONMENT, Mage::app()->getStore());
        return (ETPay_ETPay_Model_ConfigInterface::ETPAY_PRODUCTION_ENVIRONMENT == $environment) 
            ? Mage::getStoreConfig(ETPay_ETPay_Model_ConfigInterface::XML_PATH_URL_PAYMENT_PRODUCTION, Mage::app()->getStore()) . $route
            : Mage::getStoreConfig(ETPay_ETPay_Model_ConfigInterface::XML_PATH_URL_PAYMENT_SANDBOX, Mage::app()->getStore()) . $route;
    }
}
