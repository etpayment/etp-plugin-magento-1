<?php

require_once Mage::getModuleDir('controllers', 'ETPay_ETPay').DS.'AbstractReceiveController.php';

class ETPay_ETPay_ResultController extends ETPay_ETPay_AbstractReceiveController
{
    public function indexAction() {
        $done = $this->process();
        if ($done) {
            $this->_redirect('checkout/onepage/success');
        } else {
            Mage::getSingleton('checkout/session')->addError($this->__('We are sorry, something was wrong with payment. Try again or select another payment method.'));
            $this->_redirect('checkout/cart');
        }
    }

    private function process()
    {
        try {
            $signatureResponse = $this->getRequest()->getParam('jwt');

            if (is_null($signatureResponse)) {
                throw new Exception($this->__('Incorrect response from ETPay.'));
            }
    
            $session = Mage::getModel('etpay/session');
            $payload = $this->decodeJWT($signatureResponse, $session);

	    if ($payload && $payload->payment_status == true) {
                $this->processOrder($payload);
                return true;
            } else if (is_object($payload)) {
                $this->cancelOrder($payload);
            }

            return false;
        } catch (\Exception $e) {
            Mage::log($e->getMessage(), Zend_Log::CRIT, 'etpay.log');
        }
    }

   /**
     *  Puts order in Processing State and Status
     */
    private function processOrder($payload)
    {
        $orderId = $payload->merchant_order_id;
        $order = $this->getOrder($orderId);

        $message = $this->__('ETPayment Accepted. (payment_token: %s, payment_status: %s)', $payload->payment_token, $payload->payment_status);

	/** @var Mage_Sales_Model_Order_Status_History $history */
	$history = Mage::getModel('sales/order_status_history')
    		->setOrder($order)
    		->setComment($message)
    		->setData('entity_name', Mage_Sales_Model_Order::HISTORY_ENTITY_NAME);

	// EITHER model save
	$history->save();
    }

    /**
     *  Puts order in Processing State and Status
     */
    private function cancelOrder($payload)
    {
        $orderId = $payload->merchant_order_id;
        $order = $this->getOrder($orderId);

	if ($order->canCancel()) {
            try {
                $order->cancel();
                
                $order->getStatusHistoryCollection(true);

                $message = $this->__('ETPayment Canceled. (payment_token: %s, payment_status: %s)', $payload->payment_token, $payload->payment_status);

		/** @var Mage_Sales_Model_Order_Status_History $history */
        	$history = Mage::getModel('sales/order_status_history')
                	->setOrder($order)
        	        ->setComment($message)
                	->setData('entity_name', Mage_Sales_Model_Order::HISTORY_ENTITY_NAME);

        	// EITHER model save
        	$history->save();
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }

        $this->recoveryCart($order);
    }

    /**
     * @param OrderInterface $order
     */
    private function recoveryCart($order)
    {
        if ($order->getEntityId()) {
            try {
                $quote = Mage::getModel('sales/quote')->load($order->getQuoteId());
                $quote->setIsActive(1)->setReservedOrderId(null);
                $quote->save();

                Mage::getSingleton('checkout/session')->setQuoteId($order->getQuoteId());
                Mage::getSingleton('checkout/session')->unsLastRealOrderId();
            } catch (Exception $e) {
                Mage::log($e->getMessage(), Zend_Log::CRIT, 'etpay.log');
            }
        }
    }
}
