<?php

require_once Mage::getModuleDir('controllers', 'ETPay_ETPay').DS.'AbstractReceiveController.php';

class ETPay_ETPay_WebhookController extends ETPay_ETPay_AbstractReceiveController
{
    public function indexAction() {
        try {
            $rawSignatureResponse = $this->getRequest()->getRawBody();
            if (is_null($rawSignatureResponse)) {
                throw new Exception($this->__('Incorrect response from ETPay.'));
            }

            $signatureResponse = json_decode($rawSignatureResponse)->payment;
	    
	    $session = Mage::getModel('etpay/session');
            $payload = $this->decodeJWT($signatureResponse, $session);
            if ($payload->payment_status == 'success') {
                $this->processOrder($payload);
                return true;
            } else if ($payload->payment_status == 'pending') {
                $this->pendingOrder($payload);
                return false;
            } else if (in_array($payload->payment_status, array('bank_error', 'error', 'authentication_error', 'timeout', 'transaction_error'))) {
                $this->cancelOrder($payload);
                return false;
            }
        } catch (\Exception $e) {
            Mage::log($e->getMessage(), Zend_Log::CRIT, 'etpay.log');
        }
    }

    /**
     *  Puts order in Processing State and Status
     */
    private function processOrder($payload)
    {
        $orderId = $payload->merchant_order_id;
        $order = $this->getOrder($orderId);

        $state = Mage_Sales_Model_Order::STATE_PROCESSING;

        $order->setState($state, true)
              ->save();

        $message = $this->__('Order was set to Complete by our automation tool.');

	/** @var Mage_Sales_Model_Order_Status_History $history */
        $history = Mage::getModel('sales/order_status_history')
                ->setOrder($order)
		->setStatus($state)
		->setComment($message)
                ->setData('entity_name', Mage_Sales_Model_Order::HISTORY_ENTITY_NAME);

        // EITHER model save
        $history->save();
    }

    /**
     *  Puts order in Processing State and Status
     */
    private function pendingOrder($payload)
    {
        $orderId = $payload->merchant_order_id;
        $order = $this->getOrder($orderId);

        $state = Mage_Sales_Model_Order::STATE_PENDING;

	$order->setState($state, true)
              ->save();

        $message = $this->__('Webhook Pending. (payment_token: %s, payment_status: %s)', $payload->payment_token, $payload->payment_status);

	/** @var Mage_Sales_Model_Order_Status_History $history */
        $history = Mage::getModel('sales/order_status_history')
		->setOrder($order)
		->setStatus($status)		
                ->setComment($message)
                ->setData('entity_name', Mage_Sales_Model_Order::HISTORY_ENTITY_NAME);

        // EITHER model save
        $history->save();
    }

    /**
     *  Puts order in Processing State and Status
     */
    private function cancelOrder($payload)
    {
        $orderId = $payload->merchant_order_id;
        $order = $this->getOrder($orderId);

        if ($order->canCancel()) {
            try {
                $order->cancel();
                
                $order->getStatusHistoryCollection(true);

                $message = $this->__('Webhook Canceled. (payment_token: %s, payment_status: %s)', $payload->payment_token, $payload->payment_status);

		/** @var Mage_Sales_Model_Order_Status_History $history */
        	$history = Mage::getModel('sales/order_status_history')
                	->setOrder($order)
                	->setComment($message)
                	->setData('entity_name', Mage_Sales_Model_Order::HISTORY_ENTITY_NAME);

        	// EITHER model save
        	$history->save();
	    } catch (Exception $e) {
                Mage::logException($e);
            }
        }
    }
}
