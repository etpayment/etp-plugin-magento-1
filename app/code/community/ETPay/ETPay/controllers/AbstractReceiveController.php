<?php

/**
 * Class Index
 * @package ETPay\ETPay\Controller
 */
abstract class ETPay_ETPay_AbstractReceiveController extends Mage_Core_Controller_Front_Action
{
    /**
     * @var Session
     */
    private $session;

    public function preDispatch()
    {
	$this->session = Mage::getSingleton('checkout/session');   
	$this->etpaySession = Mage::getModel('etpay/session');

        parent::preDispatch();
    }

    protected function decodeJWT($signatureResponse)
    {
        $jwtValues = explode('.', $signatureResponse);
    
        $tokenHeader = $jwtValues[0];
        $tokenPayload = $jwtValues[1];
        $tokenSignature = $jwtValues[2];

        $payload = json_decode(base64_decode($tokenPayload));

        $token = $payload->session_token;

        $this->etpaySession->load($token, 'token');
        if ($this->etpaySession->getId()) {
            $signatureToken = $this->etpaySession->getSignatureToken();
        } else {
            return false;
        }

        $hash = hash_hmac('sha256', $tokenHeader . '.' . $tokenPayload, $signatureToken, true);
        $validation = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($hash));

        if ($validation == $tokenSignature) {
            return json_decode(base64_decode($tokenPayload));
        } else {
            return false;
        }
    }

    protected function getOrder($orderId = null)
    {
        if (is_null($this->order)) {
            $this->order = Mage::getModel('sales/order');
            if (is_null($orderId)) {
                $orderId = $this->session->getLastRealOrderId();
            }

            $this->order->loadByIncrementId($orderId);

            $this->session->setQuoteId($this->order->getQuoteId());
            if ($this->session->getQuote()->getIsActive()) {
                $this->session->getQuote()->setIsActive(false)->save();
            }
        }
        return $this->order;
    }
}
