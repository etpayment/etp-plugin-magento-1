<?php

$installer = $this;

$installer->startSetup();

$tableName = $installer->getTable('etpay_sessions');
$table = $installer->getConnection()
    ->newTable($tableName)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Id')
    ->addColumn('token', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
        'default' => ''
        ), 'Token')
    ->addColumn('signature_token', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
        'default' => ''
        ), 'Signature Token');

$installer->getConnection()->createTable($table);

$installer->endSetup();
